(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const blockies = require('blockies');

var icon = blockies.create({ // All options are optional
    seed: 'randstring', // seed used to generate icon data, default: random
    color: '#dfe', // to manually specify the icon color, default: random
    bgcolor: '#aaa', // choose a different background color, default: random
    size: 15, // width/height of the icon in blocks, default: 8
    scale: 3, // width/height of each block in pixels, default: 4
    spotcolor: '#000' // each pixel has a 13% chance of being of a third color,
});

document.body.appendChild(icon); // icon is a canvas element
},{"blockies":2}],2:[function(require,module,exports){
module.exports=function(){function r(r){for(var t=0;t<l.length;t++)l[t]=0;for(var t=0;t<r.length;t++)l[t%4]=(l[t%4]<<5)-l[t%4]+r.charCodeAt(t)}function t(){var r=l[0]^l[0]<<11;return l[0]=l[1],l[1]=l[2],l[2]=l[3],l[3]=l[3]^l[3]>>19^r^r>>8,(l[3]>>>0)/(1<<31>>>0)}function e(){var r=Math.floor(360*t()),e=60*t()+40+"%",o=25*(t()+t()+t()+t())+"%",n="hsl("+r+","+e+","+o+")";return n}function o(r){for(var e=r,o=r,n=Math.ceil(e/2),a=e-n,l=[],f=0;o>f;f++){for(var h=[],c=0;n>c;c++)h[c]=Math.floor(2.3*t());var i=h.slice(0,a);i.reverse(),h=h.concat(i);for(var v=0;v<h.length;v++)l.push(h[v])}return l}function n(r,t,e,o,n){var a=document.createElement("canvas"),l=Math.sqrt(r.length);a.width=a.height=l*e;var f=a.getContext("2d");f.fillStyle=o,f.fillRect(0,0,a.width,a.height),f.fillStyle=t;for(var h=0;h<r.length;h++){var c=Math.floor(h/l),i=h%l;f.fillStyle=1==r[h]?t:n,r[h]&&f.fillRect(i*e,c*e,e,e)}return a}function a(t){t=t||{};var a=t.size||8,l=t.scale||4,f=t.seed||Math.floor(Math.random()*Math.pow(10,16)).toString(16);r(f);var h=t.color||e(),c=t.bgcolor||e(),i=t.spotcolor||e(),v=o(a),u=n(v,h,l,c,i);return u}var l=Array(4);return a}();
},{}]},{},[1]);
