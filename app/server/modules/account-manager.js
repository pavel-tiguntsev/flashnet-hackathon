
const crypto 		= require('crypto');
const moment 		= require('moment');
const MongoClient 	= require('mongodb').MongoClient;

var db, accounts, contacts;
MongoClient.connect(process.env.DB_URL, function(e, client) {
	if (e){
		console.log(e);
	}	else{
		db = client.db(process.env.DB_NAME);
		accounts = db.collection('accounts');
		contacts = db.collection('contacts');
		console.log('mongo :: connected to database :: "'+process.env.DB_NAME+'"');
	}
});

/* login validation methods */

exports.autoLogin = function(user, pass, callback)
{
	accounts.findOne({user:user}, function(e, o) {
		if (o){
			o.pass == pass ? callback(o) : callback(null);
		}	else{
			callback(null);
		}
	});
}

exports.manualLogin = function(user, pass, callback)
{
	accounts.findOne({user:user}, function(e, o) {
		if (o == null){
			callback('user-not-found');
		}	else{
			validatePassword(pass, o.pass, function(err, res) {
				if (res){
					callback(null, o);
				}	else{
					callback('invalid-password');
				}
			});
		}
	});
}

exports.ethLogin = function(eth, callback)
{
	accounts.findOne({eth: eth}, function(e, o) {
		console.log(o);
		if (o == null){
			callback('user-not-found');
		}	else{
			callback(null, o);
		}
	});
}


/* record insertion, update & deletion methods */

exports.addNewAccount = function(newData, callback)
{
	accounts.findOne({user:newData.user}, function(e, o) {
		if (o){
			callback('username-taken');
		}	else{
			accounts.findOne({$or: [
				{email: newData.email},
				{eth: newData.eth}
			]}, function(e, o) {
				if (o){
					callback('email-taken');
				}	else{
					saltAndHash(newData.pass, function(hash){
						newData.pass = hash;
					// append date stamp when record was created //
						newData.date = moment().format('MMMM Do YYYY, h:mm:ss a');
						accounts.insert(newData, {safe: true}, callback);
					});
				}
			});
		}
	});
}

exports.createContact = function(newData, callback)
{
	accounts.findOne({_id: getObjectId(newData.owner)}, function(e, o) {
		var newquan = parseInt(o.battery) - parseInt(newData.quan);
		var onsale = parseInt(o.onsale) + parseInt(newData.quan);
		o.battery = newquan;
		o.onsale = onsale;
		accounts.save(o, {safe: true}, function(e) {
			if (e) callback(e);
			contacts.insert(newData, {safe: true}, callback);
		});
	})
}

exports.updateAccount = function(newData, callback)
{
	accounts.findOne({_id:getObjectId(newData.id)}, function(e, o){
		o.name 		= newData.name;
		o.email 	= newData.email;
		o.location 	= newData.location;
		o.eth 	= newData.eth;
		if (newData.pass == ''){
			accounts.save(o, {safe: true}, function(e) {
				if (e) callback(e);
				else callback(null, o);
			});
		}	else{
			saltAndHash(newData.pass, function(hash){
				o.pass = hash;
				accounts.save(o, {safe: true}, function(e) {
					if (e) callback(e);
					else callback(null, o);
				});
			});
		}
	});
}

exports.setTestSource = function(id, callback)
{
	accounts.findOne({_id: getObjectId(id)}, function(e, o){
		o.battery 	= 1000;
		accounts.save(o, {safe: true}, function(e) {
			if (e) callback(e);
			else callback(null, o);
		});

	});
}

exports.buy = function(buyer, id, callback)
{
	accounts.findOne({_id: getObjectId(buyer)}, function(e, o){
		contacts.findOne({_id: getObjectId(id)}, function(e2, o2) {
			if (o2) {
				o.purchased = o.purchased + o2.quan;
				accounts.save(o, {safe: true}, function(e) {
					contacts.remove({_id: getObjectId(id)}, callback(e,o ));
				});
			}
		})
	});
}

exports.clear = function(user, id, callback)
{
	contacts.findOne({_id: getObjectId(id)}, function(ec, oc) {
		accounts.findOne({_id: getObjectId(user)}, function(ea, oa) {
			console.log(parseInt(oa.battery), parseInt(oc.quan))
			oc.status = "filled";
			oa.battery = parseInt(oa.battery) + parseInt(oc.quan)
			accounts.save(oa, {safe: true}, function(e) {
				contacts.save(oc, {safe: true}, function(e) {
					callback(ea, oa)
				});
			});
		})
	})
}

exports.updatePassword = function(email, newPass, callback)
{
	accounts.findOne({email:email}, function(e, o){
		if (e){
			callback(e, null);
		}	else{
			saltAndHash(newPass, function(hash){
		        o.pass = hash;
		        accounts.save(o, {safe: true}, callback);
			});
		}
	});
}

/* account lookup methods */



exports.deleteAccount = function(id, callback)
{
	accounts.remove({_id: getObjectId(id)}, callback);
}

exports.getAccountByEmail = function(email, callback)
{
	accounts.findOne({email:email}, function(e, o){ callback(o); });
}

exports.validateResetLink = function(email, passHash, callback)
{
	accounts.find({ $and: [{email:email, pass:passHash}] }, function(e, o){
		callback(o ? 'ok' : null);
	});
}

exports.getAllRecords = function(callback)
{
	accounts.find().toArray(
		function(e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
}

exports.getAllContacts = function(callback)
{
	contacts.find({status: 'delployed'}).toArray(
		function(e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
}

var CronJob = require('cron').CronJob;
new CronJob('*/5 * * * * *', function() {
	contacts.updateMany({status: {$ne :"filled"}}, {$set: {status: 'delployed'}})
}, null, true, 'America/Los_Angeles');

exports.delAllRecords = function(callback)
{
	accounts.remove({}, callback); // reset accounts collection for testing //
}

/* private encryption & validation methods */

var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

var validatePassword = function(plainPass, hashedPass, callback)
{
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
	callback(null, hashedPass === validHash);
}

var getObjectId = function(id)
{
	return new require('mongodb').ObjectID(id);
}

exports.findById = function(id, callback)
{
	accounts.findOne({_id: getObjectId(id)},
		function(e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
}

var findByMultipleFields = function(a, callback)
{
// this takes an array of name/val pairs to search against {fieldName : 'value'} //
	accounts.find( { $or : a } ).toArray(
		function(e, results) {
		if (e) callback(e)
		else callback(null, results)
	});
}
