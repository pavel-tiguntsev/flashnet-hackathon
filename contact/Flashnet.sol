pragma solidity ^0.4.18;


contract Flashnet {
    uint cost;
    uint quantity;
    address delegatedTo;
    bool delegated;
    function Flashnet(
        uint etherCostOfEachkW,
        uint _quantity
    ) public {
        delegatedTo = msg.sender;
        cost = etherCostOfEachkW * quantity;
        quantity = _quantity;
        delegated = false;
    }

    function Buy() public payable returns (bool delegated) {
        if (msg.value >= cost) {
            delegatedTo = msg.sender;
            delegated = true;
            return delegated;
        } else {
            return delegated;
        }
    }
    
    function getCost() public returns (uint cost) {
        return cost;
    }
    
    function getUser() public returns (address delegatedTo) {
        return delegatedTo;
    }
    
    function getStatus() public returns (bool delegated) {
        return delegated;
    }
}
